# Cellular Braided River Modeling

This package simulates a braided river channel using a cellular routing model described by Murray and Paola (1994).

Modified from Stuart McLelland's model, originally written in VBA.  Original codebase accessed via http://www.coulthard.org.uk/downloads/murray_and_paola.htm

Translated to MATLAB and further modified by Colin Keating, Laboratory Manager, Environmental Systems Dynamics Laboratory <esdlberkeley.com>

## Getting Started

To run this package in MATLAB, ensure that all files are located in a folder in your current workspace

### Prerequisites

This package requires MATLAB.

### Running simulations

Simulations can be run by calling the main function from the MATLAB command window or by clicking the green arrow labeled "Run" located in the Editor tab.

Modify input parameters by editing values in the class "McLelland_1997_model_inputs.m"

Examples:

Run model

    `McLelland_1997`

*Note: based on which sediment transport model is specified, different values for the constant K are recommended; see below for order of magnitude guidelines*

Sediment Transport Model #1
K ~ 500

Sediment Transport Model #2
K ~ 5000-10000

Sediment Transport Model #3
K ~ 0.5

*Assuming default values for all other parameters. See comments in the "McLelland_1997_model_inputs.m" class*

## Contributors

Dr. Stuart McLelland developed the original model, which is accessible at http://www.coulthard.org.uk/downloads/murray_and_paola.htm

Colin Keating translated Dr. McLelland's original model to MATLAB and made additional modifications.

## References

http://www.coulthard.org.uk/downloads/murray_and_paola.htm

Murray and Paola, 1994. "A cellular model of braided rivers"`