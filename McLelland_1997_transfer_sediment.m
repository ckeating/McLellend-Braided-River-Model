function sediment_transferred = McLelland_1997_transfer_sediment(transport_model, ...
    Qi, K, m, Si, C, epsilon, E, Qwater, Qsed, trendz)
    if transport_model == 1
        sediment_transferred = K * (Qi ^ m);
    elseif transport_model == 2
        sediment_transferred = K * (Qi * abs(Si)) ^ m;
    elseif transport_model == 3
%         if randi(100,1) == 50
%             sprintf('abs(Si): %d', abs(Si))
%         end
        sediment_transferred = K * (Qi * (abs(Si) + C/100)) ^ m;
% In Progress: 
%     elseif model == 4
%         sediment_transferred = K * (Qi * Si + epsilon(
    else
        error('model must be integer between 1 and 4, inclusive');
    end
%     sprintf('sediment transferred: %d', sediment_transferred)
%     sprintf('slope: %d', Si)
%     sprintf('discharge: %d', Qi)
    
    if abs(sediment_transferred) > C/100
        McLelland_1997_plot(E-trendz, Qwater, Qsed, C, transport_model);
        error('Whoa, too much sediment transfer!');
    end