%braided_veg_jf_updated_constants
classdef McLelland_1997_model_inputs
    % Input parameters for McLelland 1997 model.
    properties ( Constant = true )
        Valley_Width = 22;          % default 22
        Valley_Length = 50;        % default 100
        Time_Steps = 2000;
        Topo_Slope = 0.5;           % percent, default 0.5
        Topo_Randomness = 50;        % percent, default 5
        Input_Flow_Width = 20;       % # cells, default 3
        
        % Use K ~=500 for sediment transport model 1
        % Use K ~=10000? for sediment transport model 2 
        % Use K ~=0.5, no k=~100 for sediment transport model 3?
        K = 1000;                    % default 500 for model 1
        m = 2.5;                    % default 2.5
        C = 1.5;                  % default 3 times the average slope
        epsilon = 0.3;              % default 0.3
        F = 10                    % for lateral sediment transfer rule; 
                                    % a few percent of total sediment transferred to downstream cells
        n = 1;                      % default 0.5; sometimes 1, used in discharge calculation (M&P 1994, Box 1, text)
        PlotStep = 100;            % default 1
        Sed_Transport_Model = 3;    % While 1 is most simplistic, M&P 1997 state that 2 and 3 are more realistic
        % Output_Time = 10;         % unused
    end
end

