function McLelland_1997_plot(E, Qwater, Qsed, C, Sed_Transport_Model)
    
    % make inverse colormap where high values are black
    reverse_gray = gray;
    reverse_gray = flipud(reverse_gray);


    figure(1)
    % plot E
    ax1 = subplot(1,3,1);
    imshow(E);
    axis image
    colormap(ax1, parula)
%     caxis([-50 50])
    if Sed_Transport_Model == 3
        caxis([-C/100 C/100])
    end
%     caxis([-20 20])
    colorbar
    title('Elevation')
    hold on
    
    % plot Qwater
    ax2 = subplot(1,3,2);
    imshow(Qwater);
    %axis image
    colormap(ax2, reverse_gray)
    colorbar
    title('Discharge')
    
    % plot Qsed
    ax3 = subplot(1,3,3);
    imshow(Qsed);
    %axis image
    colormap(ax3, reverse_gray)
    if Sed_Transport_Model == 3
        caxis([0 C/5000])
    end
    colorbar
    title('Sediment transfer')
    