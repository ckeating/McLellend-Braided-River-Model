function sediment_transferred = McLelland_1997_transfer_lateral_sediment(F, total_sed, slope, C, E, Qwater, Qsed, trendz, transport_model)

    sediment_transferred = F * total_sed * slope;
    
    % where 
    % slope is the lateral slope
    % total_sed is the total sediment transferred to the three downstream
    % neighbor cells
    % F is a constant such that a few percent of the total sediment is transferred laterally
  
% Debugging    
%     sprintf('total_sediment: %d', total_sed)
%     sprintf('sediment transferred: %d', sediment_transferred)
%     sprintf('slope: %d', Si)
%     sprintf('percent of total transferred laterally: %d', sediment_transferred/total_sed*100)
    
%     if abs(sediment_transferred) > total_sed/2
%         McLelland_1997_plot(E-trendz, Qwater, Qsed, C, transport_model);
%         error('greater than 50% lateral sediment transfer');
%     end