% Translation of Dr Stuart McLelland's VBA model based on Murray and Paola
% 1994.  Some additional functionality added. See README.md for more info.
function McLelland_1997()
    clear
    close all
    %% Get input arguments from an external class
    Valley_Width = McLelland_1997_model_inputs.Valley_Width;
    Valley_Length = McLelland_1997_model_inputs.Valley_Length;
    Topo_Slope = McLelland_1997_model_inputs.Topo_Slope;
    Topo_Randomness = McLelland_1997_model_inputs.Topo_Randomness;
    Input_Flow_Width = McLelland_1997_model_inputs.Input_Flow_Width;
    K = McLelland_1997_model_inputs.K;
    F = McLelland_1997_model_inputs.F;
    m = McLelland_1997_model_inputs.m;
    C = McLelland_1997_model_inputs.C;
    epsilon = McLelland_1997_model_inputs.epsilon;
    n = McLelland_1997_model_inputs.n;
    PlotStep = McLelland_1997_model_inputs.PlotStep;
    Time_Steps = McLelland_1997_model_inputs.Time_Steps;
    Sed_Transport_Model = McLelland_1997_model_inputs.Sed_Transport_Model;
    
    %% Initialize variables
    E = zeros(Valley_Length+1, Valley_Width);
    
    %% Generate input flow
    % Input flow always adds up to 1 and is divided among the cells with
    % discharge evenly.
    InputFlowQ = 1 / Input_Flow_Width;
    if mod(Input_Flow_Width,2)==1
        lower_discharge_rowindex = uint16(Valley_Width/2 - Input_Flow_Width/2);
        upper_discharge_rowindex = uint16(Valley_Width/2 + Input_Flow_Width/2-1);
    else
        lower_discharge_rowindex = uint16(Valley_Width/2 - Input_Flow_Width/2 + 1);
        upper_discharge_rowindex = uint16(Valley_Width/2 + Input_Flow_Width/2);
    end
    %% Generate starting topography

    % add overall trend
    % zmax is elevation of the top 
    zmax = 100;
    %zmin = zmax-Topo_Slope*Valley_Length;
    for row_i = 1:Valley_Length+1
        % Put in the general bed slope trend
        E(row_i,:) = zmax-Topo_Slope/100*(row_i-1); 
    end
    trendz = E;
    % Put in some topographic white noise on the order of the height 
    % difference between rows.  Avg height difference between rows is 
    % defined by the constant "s" 
    % z = trendz + sign(randn(nrows, ncolumns)) .* 200000 .* rand(nrows, ncolumns); 
    E = E + sign(randn(Valley_Length+1, Valley_Width)) .* ...
        rand(Valley_Length+1, Valley_Width) * Topo_Randomness/100 * Topo_Slope/100; 
    
    % Create high sidewalls to contain flow
    E(:,1) = 200;
    E(:,Valley_Width) = 200;
    
    % Validation Notes: 
    % Initial topography generation has been validated against 
    % spreadsheet from McLelland model
    %
    % Initial discharge generation has been validated
    
    
    %% Iterate through time steps
    for iteration_i = 1:Time_Steps

        % reset Qwater and Qsed
        % cumulative Qsed for iteration
        Qsed = zeros(Valley_Length, Valley_Width); 
        Qwater = zeros(Valley_Length, Valley_Width);
        
        Qwater(1,lower_discharge_rowindex:upper_discharge_rowindex) = InputFlowQ;
        % debugging
        %Qwater_row1 = Qwater(1,:);
        
        % Row Loop; First row contains input flow; start from second row
        %% Treats the middle downstream neighbor as row_i, column_i
        for row_i = 2:Valley_Length
            % for debugging; see how much sediment is transferred
            % laterally, versus downstream.
            lateral_sediment_total=0;
            
            % store Qsed for a row's worth data (current and next row)
            Qsed_current = zeros(Valley_Length, Valley_Width); 
            % store all sediment leaving for one row.
            sediment_out = zeros(1, Valley_Width); 
            % sprintf('row_i: %d', row_i)
            % Column Loop
            for column_i = 1:Valley_Width
                % sprintf('column_i: %d', column_i)
                % Read flow from upstream cell
                Q0 = Qwater(row_i - 1, column_i);

                % If upstream cell contains water
                if Q0 > 0

                    % Find elevations from height data array
                    Y0 = E(row_i - 1, column_i);
                    if column_i > 2
                        Yl = E(row_i, column_i - 1);
                        Ylateral_left = E(row_i-1, column_i - 1);
                    else
                        Yl = 200;
                        Ylateral_left = 200;
                    end
                    Yc = E(row_i, column_i);
                    if column_i < Valley_Width-1
                        Yr = E(row_i, column_i + 1);
                        Ylateral_right = E(row_i-1, column_i + 1);
                    else
                        Yr = 200;
                        Ylateral_right = 200;
                    end

                    % Calculate slopes (to downstream neighbors)
                    Real_Size = 1;
                    Sl = (Y0 - Yl) / (((Real_Size ^ 2) + (Real_Size ^ 2)) ^ 0.5);
                    Sc = Y0 - Yc;
                    Sr = (Y0 - Yr) / (((Real_Size ^ 2) + (Real_Size ^ 2)) ^ 0.5);
                    
                    Slateral_left = Y0 - Ylateral_left;
                    Slateral_right = Y0 - Ylateral_right;
                    
                    % calculate slopes 

                    % Check slope between cells - if any cell is a wall, then do not route water into that cell
                    if (Sl < 0 || Sc < 0 || Sr < 0) && ...
                            (~(Sl <= 0 && Sc <= 0 && Sr <= 0)) && ...
                            (~(Yl == 200 || Yc == 200 || Yr == 200))
                        %If only one or two slopes are negative then no flow is routed to that cell
                        if Sl < 0; Sl = 0; end
                        if Sc < 0; Sc = 0; end
                        if Sr < 0; Sr = 0; end
                                        
                        % Calculated routed flow from left, centre and right cells (adds routed water to existing water)
                        Ql_routed = (Q0 * (Sl ^ n / (Sl ^ n + Sc ^ n + Sr ^ n)));
                        Qc_routed = (Q0 * (Sc ^ n / (Sl ^ n + Sc ^ n + Sr ^ n)));
                        Qr_routed = (Q0 * (Sr ^ n / (Sl ^ n + Sc ^ n + Sr ^ n)));
                    
                    % else if all slopes of negative
                    elseif Sl < 0 && Sc < 0 && Sr < 0 && (~ (Yl == 200 || Yc == 200 || Yr == 200))
                        % n is inverted so that so that water flows into smallest negative slope,
                        n1 = -n;
                        n2 = -n;
                        n3 = -n;

                        % Calculated routed flow from left, centre and right cells (adds routed water to existing water)
                        Ql_routed = (Q0 * (abs(Sl) ^ n1 / (abs(Sl) ^ n1 + abs(Sc) ^ n2 + abs(Sr) ^ n3)));
                        Qc_routed = (Q0 * (abs(Sc) ^ n2 / (abs(Sl) ^ n1 + abs(Sc) ^ n2 + abs(Sr) ^ n3)));
                        Qr_routed = (Q0 * (abs(Sr) ^ n3 / (abs(Sl) ^ n1 + abs(Sc) ^ n2 + abs(Sr) ^ n3)));
                    
                    % If any slopes are zero and none are positive    
                    elseif (Sl == 0 || Sc == 0 || Sr == 0) && (Sl <= 0 && Sc <= 0 && Sr <= 0) && ...
                            (~(Yl == 200 || Yc == 200 || Yr == 200))
                        % This works out how many cells should have flow Szplit between them
                        Szplit = 1;
                        if Sl == 0
                            Sl = 1;
                            Szplit = Szplit + 1;
                        else
                            Sl = 0;
                        end
                        if Sc == 0
                            Sc = 1;
                            Szplit = Szplit + 1;
                        else
                            Sc = 0;
                        end
                        if Sr == 0
                            Sr = 1;
                            Szplit = Szplit + 1;
                        else
                            Sr = 0;
                        end

                        % Flow is Szplit between those cells with a slope of zero
                        Ql_routed = Q0 * Sl / Szplit;
                        Qc_routed = Q0 * Sc / Szplit;
                        Qr_routed = Q0 * Sr / Szplit;
                    
                    % If any cell is a wall then only route water within the central valley
                    elseif Yl == 200 || Yc == 200 || Yr == 200
                    % Route water when only an edge cell is a wall cell
                        if Yc < 200
                            % If either edge cell or the centre cell has a negative slope
                            if ((Sl < 0 && ~(Yl == 200)) || Sc < 0 || (Sr < 0 && ~(Yr == 200)))...
                                    && (~(Sl <= 0 && Sc <= 0 && Sr <= 0))
                                if Sl < 0; Sl = 0; end
                                if Sc < 0; Sc = 0; end
                                if Sr < 0; Sr = 0; end

                                % Calculated routed flow from left, centre and
                                % right cells (adds routed water to existing
                                % water).
                                Ql_routed = (Q0 * (Sl ^ n / (Sl ^ n + Sc ^ n + Sr ^ n)));
                                Qc_routed = (Q0 * (Sc ^ n / (Sl ^ n + Sc ^ n + Sr ^ n)));
                                Qr_routed = (Q0 * (Sr ^ n / (Sl ^ n + Sc ^ n + Sr ^ n)));

                            %If one edge slope and the centre slope are negative
                            elseif ((Yl == 200 && Sr < 0) || (Yr == 200 && Sl < 0)) && Sc < 0
                                % n is inverted so that so that water flows into smallest negative slope,
                                n1 = -n;
                                n2 = -n; 
                                n3 = -n;
                                if Yl == 200
                                    Ql_routed = 0;
                                    Qc_routed = (Q0 * (abs(Sc) ^ n2 / (abs(Sl) ^ n1 + abs(Sc) ^ n2 + abs(Sr) ^ n3)));
                                    Qr_routed = (Q0 * (abs(Sr) ^ n3 / (abs(Sl) ^ n1 + abs(Sc) ^ n2 + abs(Sr) ^ n3)));
                                elseif Yr == 200 
                                    Ql_routed = (Q0 * (abs(Sl) ^ n1 / (abs(Sl) ^ n1 + abs(Sc) ^ n2 + abs(Sr) ^ n3)));
                                    Qc_routed = (Q0 * (abs(Sc) ^ n2 / (abs(Sl) ^ n1 + abs(Sc) ^ n2 + abs(Sr) ^ n3)));
                                    Qr_routed = 0;
                                end

                            %If any slopes are zero
                            elseif ((Sl == 0 && ~(Yl == 200)) || ...
                                    (Sc == 0 && ~(Yc == 200)) || ...
                                    (Sr == 0 && ~(Yr == 200))) && ...
                                    (Sl <= 0 && Sc <= 0 && Sr <= 0)
                                % this works out how many cells should have flow Szplit between them
                                Szplit = 1;
                                if Sl == 0 && Yl < 200
                                    Sl = 1;
                                    Szplit = Szplit + 1;
                                else
                                    Sl = 0;
                                end
                                if Sc == 0 && Yc < 200
                                    Sc = 1;
                                    Szplit = Szplit + 1;
                                else
                                    Sc = 0;
                                end
                                if Sr == 0 && Yr < 200
                                    Sr = 1;
                                    Szplit = Szplit + 1;
                                else
                                    Sr = 0;
                                end

                                Ql_routed = Q0 * Sl / Szplit;
                                Qc_routed = Q0 * Sc / Szplit;
                                Qr_routed = Q0 * Sr / Szplit;

                            % And if there are no slope problems use the simple formula without slope adjustment
                            else
                                % Prevent flow into edge cells
                                if Yl == 200; Sl = 0; end
                                if Yr == 200; Sr = 0; end

                                Ql_routed = (Q0 * (Sl ^ n / (Sl ^ n + Sc ^ n + Sr ^ n)));
                                Qc_routed = (Q0 * (Sc ^ n / (Sl ^ n + Sc ^ n + Sr ^ n)));
                                Qr_routed = (Q0 * (Sr ^ n / (Sl ^ n + Sc ^ n + Sr ^ n)));

                            end
                        % If the center cell is a wall route all flow to the left or right
                        else
                            if Yl < 200
                                Ql_routed = Q0;
                                Qc_routed = 0;
                                Qr_routed = 0;
                            elseif Yr < 200
                                Ql_routed = 0;
                                Qc_routed = 0;
                                Qr_routed = Q0;
                            end
                        end
                        
                    % And if there are no slope problems, use the simple formulae without slope adjustment
                    else
                        Ql_routed = (Q0 * (Sl ^ n / (Sl ^ n + Sc ^ n + Sr ^ n)));
                        Qc_routed = (Q0 * (Sc ^ n / (Sl ^ n + Sc ^ n + Sr ^ n)));
                        Qr_routed = (Q0 * (Sr ^ n / (Sl ^ n + Sc ^ n + Sr ^ n)));
                    end
                    
                    % debugging
                    % Qwater(1,:)
                    % assert(sum(Qwater(1,:))==sum(Qwater_row1), ...
                    %   sprintf('error: Qwater row 1 has changed, ...
                    %   Qwater_current_row1: %d  \n\n Qwater_original_row1: %d', Qwater(1,:), Qwater_row1))
                    % Note: Qwater first row doesn't change
                    
                    % Recalculate water transport loads
                    if column_i > 2 
                        Qwater(row_i, column_i - 1) = Qwater(row_i, column_i - 1) + Ql_routed;
                    end
                    Qwater(row_i, column_i) = Qwater(row_i, column_i) + Qc_routed;
                    if column_i < Valley_Width
                        Qwater(row_i, column_i + 1) = Qwater(row_i, column_i + 1) + Qr_routed;
                    end

                    % Recalculate the sediment transport load using the
                    % appropriate transport model
                    %% sediment transport
                    % Use one of the four transport models
                    % counts total sediment transferred out of current cell
                    % transfer to left downstream neighbor
                    if column_i > 2
                        sediment_leftdownstream = ...
                            McLelland_1997_transfer_sediment(Sed_Transport_Model, Ql_routed, K, m, Sl, C, epsilon, E, Qwater, Qsed, trendz);
                        Qsed_current(row_i, column_i - 1) = Qsed_current(row_i, column_i - 1) + sediment_leftdownstream;
                        sediment_out(column_i) = sediment_out(column_i) + sediment_leftdownstream; 
                    end
                    % transfer to center downstream neighbor
                    sediment_centerdownstream = ...
                        McLelland_1997_transfer_sediment(Sed_Transport_Model, Qc_routed, K, m, Sc, C, epsilon, E, Qwater, Qsed, trendz);
                    Qsed_current(row_i, column_i) = Qsed_current(row_i, column_i) + sediment_centerdownstream;
                    sediment_out(column_i) = sediment_out(column_i) + sediment_centerdownstream;
                        
                    % transfer to right downstream neighbor
                    if column_i < Valley_Width
                        sediment_rightdownstream = ...
                            McLelland_1997_transfer_sediment(Sed_Transport_Model, Qr_routed, K, m, Sr, C, epsilon, E, Qwater, Qsed, trendz);
                        Qsed_current(row_i, column_i + 1) = Qsed_current(row_i, column_i + 1) + sediment_rightdownstream;
                        sediment_out(column_i) = sediment_out(column_i) + sediment_rightdownstream;    
                    end
                    %% lateral sediment transport
                    % Based on sum of all sediment transported out of
                    % cell, transport a fraction of the sediment
                    % laterally based on rule in Box 1, M&P 1994.
                    
                    if column_i > 2 && Slateral_left > 0
                        sediment_left_lateral = ...
                            McLelland_1997_transfer_lateral_sediment(F, sediment_out(column_i), Slateral_left, C, E, Qwater, Qsed, trendz, Sed_Transport_Model);
                        Qsed_current(row_i-1, column_i - 1) = ...
                            Qsed_current(row_i-1, column_i - 1) + sediment_left_lateral; 
                        sediment_out(column_i) = sediment_out(column_i) + sediment_left_lateral;  
                        lateral_sediment_total = lateral_sediment_total + sediment_left_lateral;
                    end
                    if column_i < Valley_Width-1 && Slateral_right > 0
                        sediment_right_lateral = ...
                            McLelland_1997_transfer_lateral_sediment(F, sediment_out(column_i), Slateral_right, C, E, Qwater, Qsed, trendz, Sed_Transport_Model);
                        Qsed_current(row_i-1, column_i + 1) = ...
                            Qsed_current(row_i-1, column_i + 1) + sediment_right_lateral; 
                        sediment_out(column_i) = sediment_out(column_i) + sediment_right_lateral;  
                        lateral_sediment_total = lateral_sediment_total + sediment_right_lateral;
                    end
                end 
            end % End Column Loop 1
        
            % Adjust the elevations in the elevation array
            for column_i = 1:Valley_Width
                Y_sed_downstream = Qsed_current(row_i, column_i);
                E(row_i, column_i) = E(row_i, column_i) + Y_sed_downstream;
                Y_sed_lateral = Qsed_current(row_i-1, column_i);
                E(row_i-1, column_i) = E(row_i-1, column_i) + Y_sed_lateral;

                % Maintain sediment input
                if row_i > 2
                    Qsed_current(row_i-1, column_i) = Qsed_current(row_i-1, column_i) - sediment_out(column_i);
                    E(row_i - 1, column_i) = E(row_i - 1, column_i) - sediment_out(column_i);
                end
%                 if mod(iteration_i,100)==0 && randi(Valley_Length/2,1)==Valley_Length/4
%                     % debugging: print sediment transferred to downstream
%                     % neighbors vs the amount transferred laterally
%                     sprintf('Total sediment transferred: %d', sum(sediment_out))
%                     sprintf('Total downstream: %d', sum(sediment_out)-lateral_sediment_total)
%                     sprintf('Total lateral: %d', lateral_sediment_total)
%                     sprintf('Lateral as percent of downstream: %d', ...
%                         lateral_sediment_total/(sum(sediment_out)-lateral_sediment_total)*100)
%                     sprintf('Lateral as percent of total: %d', ...
%                         lateral_sediment_total/sum(sediment_out)*100)
%                 end
            end % End Column Loop 2
            % reset Qsed
            Qsed = Qsed + Qsed_current;
        end     % End Row Loop
        
        %% Plot things
        if mod(iteration_i, PlotStep) == 0
            iteration_i
            McLelland_1997_plot(E-trendz, Qwater, Qsed, C, Sed_Transport_Model);
            pause(1)
            
        end
        %Qwater
    end         % End Iteration 
    
    sprintf('DONE')